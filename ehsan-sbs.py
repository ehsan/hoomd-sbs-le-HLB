#! /usr/bin/env python3

import hoomd
import numpy as np, os, datetime, time, json
import saw
import hoomd_sbs_utils as hsu
from hoomd import md
import argparse

##############################
## command line arguments ##
##############################
parser = argparse.ArgumentParser()
args = hsu.add_arguments(parser)
with open(args.simp_fname) as simp_f:
    simp = json.load(simp_f)
N_beads = simp['N']
L = np.array(simp['L'])
for l in range(len(L)):
    if L[l]==0: L[l] = 1*N_beads**0.5887
Lhalf = L*0.5
dt = simp['dt']  # the integration time step
KT = simp['KT']    # temperature
N_binders = simp['N_binders']
particle_types = simp['particles']['types']
bond_types = simp['bonds']['types']
sim_id = args.sim_id
if args.hoomd_args=='cpu':
    hoomd.context.initialize('--mode=cpu')
else:
    hoomd.context.initialize('--mode={}'.format(args.hoomd_args))
   
##############################
## start or restart? ##
##############################
restart_fname = '{}-simid-{}.gsd'.format(simp['restart_fname'], args.sim_id)
if os.path.exists(restart_fname):
    # read the restart file
    system = hoomd.init.read_gsd(filename=restart_fname, restart=restart_fname)
    restart = True # flag the simulation mood!
else:
    restart = False # flag the simulation mood!
    snapshot = hoomd.data.make_snapshot(N=N_beads+N_binders, box=hoomd.data.boxdim(*L), 
            particle_types=particle_types, bond_types=bond_types)
    snapshot = hsu.set_snapshot2 (snapshot, sim_params=simp)
    system = hoomd.init.read_snapshot(snapshot) # read the first snapshot

# Neighborlist
nl = md.nlist.cell(check_period=1)
nl.set_params(r_buff=0.3)
nl.reset_exclusions(['1-2'])
nl.tune()
##############################
## removing overlaps ##
##############################
rem_overlap_store = {}
rem_overlap_store['pairs'] = {}
rem_overlap_store['bonds'] = {}
hsu.set_features(simp['remove_overlapps'], store = rem_overlap_store['pairs'], feature='pairs', md=md, nl=nl)
hsu.set_features(simp['remove_overlapps'], store = rem_overlap_store['bonds'], feature='bonds', md=md, nl=nl)
all = hoomd.group.all()
if not(restart):
    nve=md.integrate.nve(group=all)
    fire = md.integrate.mode_minimize_fire(dt=0.001, ftol=1e-5, Etol=1e-7)
    while not(fire.has_converged()):
        hoomd.run(5000)
    nve.disable()
for k,pair0 in rem_overlap_store['pairs'].items():
    pair0.disable()
##############################
## normal MD: thermalization ##
##############################
thermal_store = {}
thermal_store['pairs'] = {}
hsu.set_features(simp['thermal'], store=thermal_store['pairs'], feature='pairs', md=md, nl=nl)
normal_md = md.integrate.mode_standard(dt=dt)
langevin = md.integrate.langevin(group=all, kT=1.0*KT, seed=np.random.randint(1,100000+1))
langevin.set_gamma(snapshot.particles.types, gamma=2)

log_fname = '{}-id-{}.log'.format(simp['log_fname'], args.sim_id)
dump_fname = '{}-id{}.gsd'.format(simp['dump_fname'], args.sim_id)
thermo = hoomd.analyze.log(filename=log_fname, quantities=['potential_energy', 'temperature'], period=simp['log_period'], overwrite=not(restart),phase=0)
dump_gsd = hoomd.dump.gsd(dump_fname, period=simp['dump_period'], group=all, overwrite=not(restart), phase=0, dynamic=['attribute','momentum','topology','property'])
restart_gsd = hoomd.dump.gsd(filename=restart_fname, group=all, truncate=True, period=simp['restart_period'], phase=0)
hoomd.run_upto(int(simp['thermal_timesteps']))
##############################
## normal MD: attraction ##
##############################
## attraction
main_md_store = {}
main_md_store['pairs'] = thermal_store['pairs']
hsu.set_features(simp, store=main_md_store['pairs'], feature='pairs', md=md, nl=nl)

############
hoomd.run(simp['run_timesteps'])
restart_gsd.write_restart()
