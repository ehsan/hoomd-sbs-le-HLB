#!/bin/bash

#$ -l data
#$ -l h_gpu=1
#$ -cwd
#$ -V
#$ -m be
#$ -l m_mem_free=10G
#$ -l h_rt=44:00:00
#$ -now n

GUIX_PROFILE="/gnu/var/guix/profiles/per-user/eirani/guix-profile"
. "$GUIX_PROFILE/etc/profile"

for id in $(seq $1 $2); do singularity exec -B /data:/data --nv ~/softwares/ehsan-singularity-hoomdv2.8.1.simg python3 ./HLB-sbs-smooth-wall.py --settings settings.HLB_settings_onlyActive.py --sim-id onlyActive-$id --hoomd 'gpu' >> simulations/onlyActive/logs/onlyActive_o_$1_$2; done

