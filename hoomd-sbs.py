#! /usr/bin/env python

import hoomd
import numpy as np, os, datetime, time, json
import saw, hoomd_sbs_utils
from hoomd import md
import argparse

##############################
## command line arguments ##
##############################
parser = argparse.ArgumentParser()
args = hoomd_sbs_utils.add_arguments(parser)
with open(args.sim_params_fname) as sim_params_f:
    sim_params = json.load(sim_params_f)

N_beads = sim_params['N']
N_binders = sim_params['N_binders']
particle_types = [sim_params[k]['name'] for k in sim_params.keys() if (len(k)>3 and k[:3]=='pt-')]
R_beads = 0.5  # beads radius
sigma = 2*R_beads   # Not clear? the diameter of beads!
D = args.D
if D==0.0:
    D = 2*sigma*(N_beads**0.5887)  # the size of the simulation box
sim_id = args.sim_id

##############################
## simulation parameters ##
##############################
Dhalf = 0.5*D
dt = args.dt  # the integration time step
KT = 1.0    # temperature
dump_period0 = 1e2
if args.hoomd_args=='cpu':
    hoomd.context.initialize('--mode=cpu')
else:
    hoomd.context.initialize('--mode={}'.format(args.hoomd_args))
    
##############################
## start or restart? ##
##############################
sim_inf='N{}-Nb{}-D{:.2f}'.format(N_beads, N_binders, D, args.thermal_time, args.run_time)
restart_fname = '{}/restart-{}-{}.gsd'.format(args.restart_path, args.sim_id, sim_inf)
if os.path.exists(restart_fname):
    # read the restart file
    system = hoomd.init.read_gsd(filename=restart_fname, restart=restart_fname)
    restart = True # flag the simulation mood!
else:
    restart = False # flag the simulation mood!
    print(N_beads, N_binders)
    print(particle_types)
    snapshot = hoomd.data.make_snapshot(N=N_beads+N_binders,
            box=hoomd.data.boxdim(L=D),
            particle_types=particle_types,
            bond_types=['polymer'])
    ## read the SAW configuration from the input gsd file
    #saw_gsd = hoomd.data.gsd_snapshot(args.saw_gsd)
    # set the snapshot properties
    snapshot = hoomd_sbs_utils.set_snapshot (snapshot, N_beads = N_beads, N_binders=N_binders,
            sigma=sigma, Dhalf=Dhalf, 
            sim_params=sim_params)
    system = hoomd.init.read_snapshot(snapshot) # read the first snapshot

# Neighborlist
nl = md.nlist.cell(check_period=1)
nl.set_params(r_buff=0.3)
nl.reset_exclusions(['1-2'])
nl.tune()
##############################
## DPD and FENE ##
##############################
dpd = md.pair.dpd(r_cut=1.0, nlist=nl, kT=0.0*KT, seed=1)
dpd.pair_coeff.set(particle_types, particle_types, A=30, gamma=2.0)
fene = md.bond.fene()
fene.bond_coeff.set('polymer', k=30.0*KT, r0=1.6*sigma, sigma=sigma, epsilon=1)
all = hoomd.group.all()
##############################
## removing overlaps ##
##############################
remove_overlapp_fname='{}/rem-overlapp-{}-{}.gsd'.format(args.remove_overlapp_path, args.sim_id, sim_inf)
gsd1=hoomd.dump.gsd(remove_overlapp_fname, group=all, period=dump_period0, overwrite=not(restart), phase=-1)
if not(restart):
    nve=md.integrate.nve(group=all)
    fire = md.integrate.mode_minimize_fire(dt=0.001, ftol=1e-5, Etol=1e-7)
    while not(fire.has_converged()):
        hoomd.run(5000)
    nve.disable()
gsd1.disable()
##############################
## normal MD: thermalization ##
##############################
dpd.disable()
## Shifted LJ: WCA repulsive potential
r_cut=sigma*2**(1./6.)
rc_rep = r_cut
rc_att = 1.5
lj = md.pair.lj (r_cut=r_cut, nlist=nl)
lj.set_params (mode="shift")
lj.pair_coeff.set(particle_types, particle_types, epsilon=1, sigma=sigma, r_cut=r_cut)

normal_md = md.integrate.mode_standard(dt=dt)
langevin  = md.integrate.langevin(group=all, kT=1.0*KT, seed=np.random.randint(1,100000+1))
langevin.set_gamma(particle_types, gamma=2)

log_fname = '{}/{}-N{}-Nb{}-thermo.log'.format(args.log_path, args.sim_id, N_beads, N_binders)
thermo = hoomd.analyze.log(filename=log_fname,
                  quantities=['potential_energy', 'temperature'],
                  period=args.log_period,
                  overwrite=not(restart),
                  phase=0)

dump_gsd = hoomd.dump.gsd(args.dump_fname, period=args.dump_period, group=all, overwrite=not(restart), phase=0, dynamic=['attribute','momentum'])
restart_gsd = hoomd.dump.gsd(filename=restart_fname, group=all, truncate=True, period=args.restart_period, phase=0)
hoomd.run_upto(int(args.thermal_time))
##############################
## normal MD: attraction ##
##############################
## attraction
print('particle types:', particle_types)
for particle_type0 in particle_types:
    k = 'pt-'+particle_type0
    interactions = [k0 for k0 in sim_params[k].keys() if "interaction-" in k0]
    if len(interactions)>0:
        for interaction0 in interactions:
            pt2 = interaction0[12:]
            pt_epsilon = sim_params[k][interaction0]['epsilon']
            pt_sigma = sim_params[k]['sigma']
            lj.pair_coeff.set(particle_type0, pt2, epsilon=pt_epsilon, sigma=pt_sigma, r_cut=rc_att)
            print (particle_type0, pt2, pt_epsilon)
############
final_timestep = args.run_time
hoomd.run(final_timestep)
restart_gsd.write_restart()
