import hoomd, saw
import numpy as np
import argparse

def set_snapshot (snapshot, N_beads, N_binders, sigma, Dhalf, sim_params):
    D=2*Dhalf
    snapshot.particles.diameter[:] = sigma
    ## loop over the particle_types
    particle_types = [sim_params[k]['name'] for k in sim_params.keys() if (len(k)>3 and k[:3]=='pt-')]
    binder_position = N_beads
    for pt_count, particle_type0 in enumerate(particle_types):
        if sim_params['pt-'+particle_type0]['chain']==1: # it is a bead type!
            ids = sim_params['pt-'+particle_type0]['ids']
            snapshot.particles.typeid[ids] = pt_count   # beads
        else:
            n_binders = sim_params['pt-'+particle_type0]["N"]
            snapshot.particles.typeid[binder_position:binder_position+n_binders] = pt_count # binders
            binder_position += n_binders # update binder_position

    # for beads on the chain:
    snapshot.particles.charge[:N_beads] = 1 # on the chain
    snapshot.bonds.resize(N_beads-1)    # set the number of bonds to N_beads-1
    
    beads_position, beads_img = saw.get_init_pol_xyz (N=N_beads, sigma=sigma, D=D)
    for bead_id in range(0, N_beads):
        pos = beads_position[bead_id]
        img = beads_img[bead_id]
        #pos = saw_gsd.particles.position[bead_id] + saw_gsd.particles.image[bead_id]*saw_gsd.box.Lx
        #img = np.zeros_like(pos)
        #img = saw_gsd.particles.image[bead_id]
        for i in range(len(pos)):
            if pos[i]>Dhalf:
                pos[i] -= D
                img[i] += 1
            elif pos[i]<=-Dhalf:
                pos[i] += D
                img[i] -= 1
        snapshot.particles.position[bead_id] = pos
        #snapshot.particles.image[bead_id] = img
        
        #snapshot.particles.image[bead_id] = saw_gsd.particles.image[bead_id]
        out_box = np.abs(pos)>Dhalf
        if (bead_id < N_beads-1):
            snapshot.bonds.group[bead_id] = [bead_id, bead_id+1] #connect particles through bonds!

    # set binders positions
    snapshot.particles.charge[N_beads:] = 2  # free
    for binder_id in range(N_beads, N_beads+N_binders):
        snapshot.particles.position[binder_id] = np.random.rand(3)*D - Dhalf

    return snapshot
def set_snapshot2 (snapshot, sim_params):
    import pandas as pd
    simp = sim_params
    D = np.array(simp['L'])
    Dhalf = 0.5*D
    N_beads = simp['N']
    N_binders = simp['N_binders']
    particle_types = simp['particles']['types']
    bond_types = simp['bonds']['types']
    table_names = ['particles', 'bonds']
    df = pd.read_csv(simp['polymer_file'], delim_whitespace=True, names=range(5))
    groups = df[0].isin(table_names).cumsum()
    tables = {g.iloc[0,0]: g.iloc[1:].dropna(axis=1).set_index(0) for k,g in df.groupby(groups)}
    beads = tables['particles']
    beads_type_col = simp['particles']['types_col']
    beads_type_id = [particle_types.index(x) for x in beads[beads_type_col].values]
    bonds = tables['bonds']
    bonds_type_col = simp['bonds']['types_col']
    bonds_type_id = [bond_types.index(x) for x in bonds[bonds_type_col]]

    snapshot.particles.typeid[:] = beads_type_id
    # for beads on the chain:
    snapshot.particles.charge[:N_beads] = 1 # on the chain
    beads_position, beads_img = saw.get_init_pol_xyz (N=N_beads, sigma=simp['sigma'], D=D)
    unwrapped_position = beads_position + beads_img*D
    rcm = np.mean(unwrapped_position, axis=0)
    central_pos = unwrapped_position - rcm
    snapshot.particles.position[:N_beads] = central_pos
    snapshot.particles.image[:N_beads] = beads_img

    # for bonds
    snapshot.bonds.resize(len(bonds))
    snapshot.bonds.group[:] = bonds.iloc[:,[0,1]]
    snapshot.bonds.typeid[:] = bonds_type_id
        
    # set binders positions
    snapshot.particles.charge[N_beads:] = 2  # free
    for binder_id in range(N_beads, N_beads+N_binders):
        snapshot.particles.position[binder_id] = np.random.rand(3)*D - Dhalf

    return snapshot
#################################
def set_features (simp, store, feature, md, nl):
    KT = 1
    particle_types=['A', 'B', 'A_binder', 'B_binder']
    sigma=1
    for f0 in simp[feature]:
        if not(f0['name'] in store.keys()):
            store[f0['name']] = eval(f0['init'])
        cmd1 = ''.join(f0['cmds'])
        print(cmd1)
        exec(cmd1)
        #for cmd0 in f0['cmds']:
        #    exec('store[f0[\'name\']].{}'.format(cmd0), globals(), locals())

#################################
def add_arguments (parser):
    parser.add_argument('--sim-params', default='sim-params.json', dest='simp_fname')
    parser.add_argument('--D', default=0, type=float, dest='D')
    parser.add_argument('--sim-id', '--sim_id', default='', dest='sim_id')
    parser.add_argument('--run-time', '--run_time', default=5e6, type=float, dest='run_time')
    parser.add_argument('--thermalize-time', '--thermal_time', default=1e7, type=float, dest='thermal_time')
    parser.add_argument('--dump-fname', '--dump_fname', default='trajectories.gsd', dest='dump_fname')
    parser.add_argument('--dump-period', '--dump_period', default=5e4, type=float, dest='dump_period')
    parser.add_argument('--restart-path', '--restart_path', default='restart/', dest='restart_path')
    parser.add_argument('--restart-period', '--restart_period', default=5e6, type=float, dest='restart_period')
    parser.add_argument('--remove-overlapp-path','--rop','--remove_overlapp_path', default='./', dest='remove_overlapp_path')
    parser.add_argument('--log-path', '--log_path', default='logs/', dest='log_path')
    parser.add_argument('--log-period', '--log_period', default=5e4, type=float, dest='log_period')
    parser.add_argument('--dt', default=0.012, type=float, dest='dt')
    parser.add_argument('--epsilon', default=12.0, type=float, dest='epsilon')
    parser.add_argument('--hoomd','--hoomd_args', default='cpu', dest='hoomd_args', type=str)
    parser.add_argument('--table', default=False, dest='table', type=bool)
    args = parser.parse_args()
    return args

##############################
## logging functions ##
##############################
def calc_R2(timstep):
    snap = system.take_snapshot(particles=True, bonds=False, integrators=False)
    ids = snap.particles.charge == 1
    res = saw.R2(snap.particles.position[ids], snap.particles.image[ids], D)
    return res

def calc_R_G2(timestep):
    snap = system.take_snapshot(particles=True, bonds=False, integrators=False)
    ids = snap.particles.charge == 1
    res = saw.R_G2(snap.particles.position[ids], snap.particles.image[ids], D)
    return res


