import numpy as np, inspect, pandas as pd
import hoomd, ehsan_loopextrusion


##############################
## Simulation parameters
##############################
N = 0
thermal_timesteps = 2e7
final_timesteps = 6e7
dump_period = 5e4
restart_period = 5e6
dump_fname = r'simulations/LEtex5e4/gsds/dump' #'gsds-smoothwall-flow-LEtex5e4/dump'
restart_fname = r'simulations/LEtex5e4/gsds/restart' #gsds-smoothwall-flow-LEtex5e4/restart'
dt = 0.01
KT = 1
L = np.array([180,180,180])
Lhalf = 0.5*L
Dhalf = Lhalf
box = hoomd.data.boxdim(*L)
sigma = 1.0
# epsilon = 8
epsilon = 3.1
flow_fvec = (0, 0, 0.05)

# LE parameters
N_le_agents = 40
le_period = 5e4
change_le_period = 5e5
le_k_new = 0.2
le_k_remove = 0.2
le_extr_dist_thr = -1
le_type = 'loop_extrusion'

# parameters for changing particles types.
# check also relevant callback function below.
change_period = 10000
change_rate = 0.01/change_period # N_change/(N_tot*change_period)
##############################
## particle, bond and angle types
## read the particle information from a file
##############################
typeid_fname = 'input_class_1kbwindow_mm9myclass_merged_byname.bed'
polymer_df = pd.read_csv(typeid_fname, delim_whitespace=True, header=None, names=['pid', 'type'])
polymer_particle_types = list(set(polymer_df.type.values))
binder_particle_types = ['%s_binder'%x for x in polymer_particle_types]
wall_particle_types = ['laminar']
particle_types = polymer_particle_types + binder_particle_types + wall_particle_types + ['LADnull']

bond_types = ['polymer', 'harmonic', 'loop_extrusion']
le_typeid = bond_types.index(le_type)
angle_types = ['polymer']

snap_args = {'N':N , 'box': box, 'particle_types': particle_types, 'bond_types': bond_types, 'angle_types': angle_types}

## define polymers 
polymers = []
pol_size = 6000
for i in range(1):
    particles_typeid = np.array([particle_types.index(x) for x in polymer_df.type.values])
    pol0 = dict(size=pol_size, bonds_typeid=0, particles_typeid=particles_typeid, pbc=np.array([True, True, False]), rcm_pos=np.zeros(3), angles_typeid=0)
    polymers.append(pol0)

## define binders
binder_sizes = [int(0.5*np.sum(particles_typeid==particle_types.index(x))) for x in polymer_particle_types]
print('binder_sizes', binder_sizes)
binder_sizes[binder_particle_types.index('Inert_binder')] = 0
#binder_sizes[binder_particle_types.index('LADs_binder')] = 0
binders = [dict(size=binder_sizes[binder_particle_types.index(bt)], particles_typeid=particle_types.index(bt)) for bt in binder_particle_types]
print(binders)

## pair potentials
r_cut = sigma*2**(1./6.)
r_cut_att = sigma*2. #sigma*1.8
pairs = {
        'dpd': {'init_cmd': 'hoomd.md.pair.dpd (r_cut=sigma, kT=0.0, nlist=nl, seed=1)'
               },
        'lj' : {'init_cmd': 'hoomd.md.pair.lj (r_cut=%f, nlist=nl)'%r_cut
               }
        }

bonds = {
        'fene': {'init_cmd': 'hoomd.md.bond.fene()',
                 'coeff_cmds': [ dict(type='polymer', k=30.0*1, r0=1.6*sigma, sigma=sigma, epsilon=1),
                                 dict(type='harmonic', k=0*1, r0=1.6*sigma, sigma=sigma, epsilon=1),
                                 dict(type='loop_extrusion', k=0*1, r0=1.1*sigma, sigma=sigma, epsilon=1) ]
                },
        'link': {'init_cmd': 'hoomd.md.bond.harmonic()',
                 'coeff_cmds': [ dict(type='polymer', k=0.0*1, r0=1.6*sigma),
                                 dict(type='harmonic', k=0*1, r0=1.1*sigma),
                                 dict(type='loop_extrusion', k=50*1, r0=1.1*sigma) ]
        #'link': {'init_cmd': 'hoomd.md.bond.fene()',
        #         'coeff_cmds': [ dict(type='polymer', k=0.0*1, r0=1.6*sigma, sigma=sigma, epsilon=1),
        #                         dict(type='harmonic', k=0*1, r0=1.6*sigma, sigma=sigma, epsilon=1),
        #                         dict(type='loop_extrusion', k=20*1, r0=1.6*sigma, sigma=sigma*1., epsilon=1) ]
                },
        }

angles = {
        'harmonic': {'init_cmd': 'hoomd.md.angle.harmonic()',
                     'coeff_cmds': [ dict(type='polymer', k=3*KT, t0=np.pi) ]
                    }
        }

system = None
orig_typeids = None
stop_probs = np.genfromtxt('input_class_1kbwindow_mm9myclass_CTCF.bed')[:,1:]
##############################
## Walls
##############################
repulsive_walls = hoomd.md.wall.group()
attractive_walls = hoomd.md.wall.group()
repulsive_walls.add_plane(origin=(0,0,-Lhalf[2]), normal=(0,0,1))
attractive_walls.add_plane(origin=(0,0,Lhalf[2]), normal=(0,0,-1))
#repulsive_walls.append(hoomd.md.wall.sphere(origin=(0,0,0), r=Dhalf[0]))

##############################
## Simulation states
##############################
particle_groups = {}
flow_force = {}
def init_remove_overlaps(main):
    main['pairs']['dpd'].pair_coeff.set(particle_types, particle_types, A=30, gamma=2.0)
    main['pairs']['lj'].disable()

    lj_wall_att = main['lj_wall_att']
    lj_wall_rep = main['lj_wall_rep']

    lj_wall_att.force_coeff.set(particle_types, sigma=1.0, epsilon=1.0, r_cut=r_cut)
    lj_wall_rep.force_coeff.set(particle_types, sigma=1.0, epsilon=1.0, r_cut=r_cut)
    lj_wall_att.force_coeff.set(binder_particle_types, sigma=1.0, epsilon=epsilon, r_cut=False)
    lj_wall_rep.force_coeff.set(binder_particle_types, sigma=1.0, epsilon=epsilon, r_cut=False)

    particle_groups['all'] = hoomd.group.all()
    particle_groups['polymer'] = hoomd.group.tags(name='polymer', tag_min=0, tag_max=5999)
    particle_groups['LADs_binder'] = hoomd.group.type(name='g-LADs_binders', type='LADs_binder', update=True)
    particle_groups['Active-LADs_binder'] = hoomd.group.type(name='g-Active-LADs_binders', type='Active-LADs_binder', update=True)
    return dict(group=particle_groups['all'], limit=0.01)

def init_thermalization(main):
    """
    prepare the pair potentials
    return the group of particles for 
    the langevin integrator
    """


    lj_wall_att = main['lj_wall_att']
    lj_wall_rep = main['lj_wall_rep']

    lj_wall_att.force_coeff.set(particle_types, sigma=1.0, epsilon=1.0, r_cut=r_cut)
    lj_wall_rep.force_coeff.set(particle_types, sigma=1.0, epsilon=1.0, r_cut=r_cut)
    lj_wall_att.force_coeff.set(binder_particle_types, sigma=1.0, epsilon=epsilon, r_cut=False)
    lj_wall_rep.force_coeff.set(binder_particle_types, sigma=1.0, epsilon=epsilon, r_cut=False)
    
    main['pairs']['dpd'].disable()
    main['pairs']['lj'].enable()
    main['pairs']['lj'].pair_coeff.set(particle_types, particle_types, epsilon=1, sigma=sigma)
    #######
    #######
    # loop extrusion

    #######
    return particle_groups['all']

def init_main_md(main):
    ## attraction
    lj = main['pairs']['lj']
    lj_wall_att = main['lj_wall_att']
    lj_wall_rep = main['lj_wall_rep']

    lj.set_params(mode='shift')
    for p in polymer_particle_types:
        lj.pair_coeff.set(p, p+'_binder' , epsilon=epsilon, sigma=1.0, r_cut=r_cut_att)
    main['pairs']['lj'].pair_coeff.set('Inert', particle_types, epsilon=1, sigma=1.0, r_cut=r_cut)
    main['pairs']['lj'].pair_coeff.set('K27-Active', ['Active_binder', 'Active-LADs_binder'], epsilon=epsilon, sigma=sigma, r_cut=r_cut_att)
    main['pairs']['lj'].pair_coeff.set('Active', ['K27-Active_binder, Active-LADs_binder'], epsilon=epsilon, sigma=sigma, r_cut=r_cut_att)
    main['pairs']['lj'].pair_coeff.set(['LADs', 'Active-LADs'], ['LADs_binder', 'Active-LADs_binder'], epsilon=epsilon, sigma=sigma, r_cut=r_cut_att)

    lj_wall_att.force_coeff.set('LADs', sigma=1.0, epsilon=epsilon, r_cut=r_cut_att)
    lj_wall_att.force_coeff.set('Active-LADs', sigma=1.0, epsilon=epsilon, r_cut=r_cut_att)
    lj_wall_att.force_coeff.set(binder_particle_types, sigma=1.0, epsilon=epsilon, r_cut=False)
    lj_wall_rep.force_coeff.set(binder_particle_types, sigma=1.0, epsilon=epsilon, r_cut=False)

    ## flow
    #for bt in ['LADs_binder', 'Active-LADs_binder']: # it was a mistake! The flow should only apply on the things which ar attached to LADs.
    for bt in ['LADs_binder']:
    	flow_force[bt] = hoomd.md.force.constant(fvec=flow_fvec, group=particle_groups[bt])


    return 0

##############################
## Callbacks 
##############################
def change_types (snap, p1_types, p2, rate, replace=False, orig_typeids=None):
    ids = np.zeros(snap.particles.N)
    for bt in p1_types:
        #ids = ids + (snap.particles.typeid == particle_types.index(bt)).astype(int)
        ids = ids + (orig_typeids == particle_types.index(bt)).astype(int)
    ids = ids.astype(bool)
    #orig_typeids = snap.particles.typeid[:]

    null_indices = np.arange(snap.particles.N)[orig_typeids==particle_types.index(p2)]
    all_binders_indices = np.arange(snap.particles.N)[ids]
    non_null_indices = np.array(list(set(all_binders_indices) - set(null_indices)))
    change_number = int(rate*(len(non_null_indices))*change_period)

    new_typeids = orig_typeids.copy()
    min_n = np.min([len(null_indices), len(non_null_indices)])
    if (replace==False)and(min_n>=change_number):
        a1, a2 = np.random.choice(null_indices, change_number), np.random.choice(non_null_indices, change_number)
        new_typeids[a1], new_typeids[a2] = new_typeids[a2], new_typeids[a1]
        #print(a1, a2)
    #else:
    if (replace==True)or((min_n+1)<change_number):
        a1 = np.random.choice(non_null_indices, change_number)
        new_typeids[a1] = particle_types.index(p2)

    snap.particles.typeid[:] = new_typeids
    return snap

def change_binders(timestep):
    snap = system.take_snapshot(all=True)
    snap = change_types (snap, binder_particle_types, 'Inert_binder', rate=change_rate, orig_typeids = orig_typeids, replace=True)
    snap = change_types (snap, ['Active-LADs', 'LADs'], 'Inert', rate=change_rate, replace=True, orig_typeids=snap.particles.typeid)
    system.restore_snapshot(snap)

