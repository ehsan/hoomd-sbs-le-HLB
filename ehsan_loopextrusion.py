import numpy as np

class LEagents:
    """ """
    def __init__ (self, system=None, stop_probs=None, N_le=0, k_new=0, k_remove=0,\
                    update_period=1, change_period=1, move_dist_thr = 1.2,
                    sl_typeid=1, sl_type='loop_extrusion', passing_p=1.0):
        self.system = system
        self.box = system.box
        self.L = np.array([self.box.Lx, self.box.Ly, self.box.Lz])
        self.Lhalf = 0.5*self.L
        self.stop_probs = stop_probs
        self.k_new = k_new
        self.k_remove = k_remove
        self.update_period = update_period
        self.change_period = change_period
        self.move_dist_thr = move_dist_thr
        self.le_typeid = sl_typeid
        self.le_type = sl_type
        self.N_le = N_le
        self.new_le_N = self.get_new_le_N()
        self.remove_le_N = self.get_remove_le_N()
        self.passing_p = passing_p

    def get_new_le_N (self):
        return int(self.k_new*self.N_le)

    def get_remove_le_N (self):
        return int(self.k_remove*self.N_le)
        
    def set_attr(self):
        return None

    def dist(self, p1, p2):
        """return a distance of two particles in the simulation box"""

        dr = np.array(p1.position) - np.array(p2.position)
        for i in range(3):
            if dr[i]>self.Lhalf[i]:
                dr[i] -= self.L[i]
            elif dr[i]<=-self.Lhalf[i]:
                dr[i] += self.L[i]
        dr_abs = np.linalg.norm(dr)
        return dr_abs

    def dist_arr(self, snap, p_ids):
        """ return the distance array of list of particles
        from a snapshot. indices of particles are passed by 
        p_ids. """

        p2 = snap.particles.position[p_ids[:, 1]]
        p1 = snap.particles.position[p_ids[:, 0]]
        dr = p1 - p2
        dr = dr - (dr/self.Lhalf).astype(int)*self.L

        dr_abs = np.linalg.norm(dr, axis=1)
        return dr_abs
    ##############################
    def remove_loop_extrusion(self, timestep):
        # snap = system.take_snapshot(particles=True, bonds=True, integrators=False)
        system = self.system
        remove_le_N = self.remove_le_N
        tags = []
        for b in self.system.bonds:
            if b.typeid != self.le_typeid:
                continue
            tags.append(b.tag)
        if len(tags) > 0:
            to_be_deleted = np.random.choice(tags, remove_le_N, replace=False)
            # print(to_be_deleted)
            if len(to_be_deleted) > 0:
                for t0 in to_be_deleted:
                    system.bonds.remove(t0)
            self.N_le = len(tags) - len(to_be_deleted)
    ##############################

    def create_new_loop_extrusion(self, timstep):
        system = self.system
        N_beads = len(self.stop_probs)
        beads_list = np.arange(N_beads)
        new_le_N = self.new_le_N
        tags = []
        for b in self.system.bonds:
            if b.typeid != self.le_typeid:
                continue
            tags.append(b.tag)
        ab_list = np.zeros((len(tags), 2))
        for i, t in enumerate(tags):
            b = system.bonds.get(t)
            ab_list[i, 0] = b.a
            ab_list[i, 1] = b.b
        # p = np.ones(N_beads)
        options = beads_list[:-1]
        if len(ab_list) > 0:
            le_lengths = np.diff(ab_list)[:, 0]
            not_permitted_a = ab_list[le_lengths == 1][:, 0].tolist()
            if len(not_permitted_a) > 0:
                options = [x for x in beads_list[:-1] if x not in not_permitted_a]
        new_a = np.random.choice(options, new_le_N, replace=False)
        for a0 in new_a:
            self.system.bonds.add(self.le_type, a0, a0+1)

        self.N_le = len(tags) + len(new_a)

    ##############################
    def create_new_loop_extrusion2(self, timstep):
        system = self.system
        N_beads = len(self.stop_probs)
        beads_list = np.arange(N_beads)
        new_le_N = self.new_le_N
        tags = []
        for b in system.bonds:
            if b.typeid != self.le_typeid:
                continue
            tags.append(b.tag)
        ab_list = np.zeros((len(tags), 2))
        for i, t in enumerate(tags):
            b = self.system.bonds.get(t)
            ab_list[i, 0] = b.a
            ab_list[i, 1] = b.b
        # p = np.ones(N_beads)
        options = beads_list[:-1]
        if len(ab_list) > 0:
            le_lengths = np.diff(ab_list)[:, 0]
            not_permitted_a = ab_list[le_lengths == 1][:, 0].tolist()
            # print('not_permitted_a:', not_permitted_a)
            if len(not_permitted_a) > 0:
                options = [x for x in beads_list[:-1] if x not in not_permitted_a]
        new_a = np.random.choice(options, new_le_N, replace=False)
        for a0 in new_a:
            self.system.bonds.add(self.le_type, a0, a0+1)

    ##############################
    def replace_loop_extrusion_factors(self, timestep):
        """
        Replace current LE factors with new ones, according to given rates.
        """
        # Removing part
        system = self.system
        remove_le_N = self.remove_le_N
        tags = []
        for b in self.system.bonds:
            if b.typeid != self.le_typeid:
                continue
            tags.append(b.tag)
        if len(tags) > 0:
            to_be_deleted = np.random.choice(tags, remove_le_N, replace=False)
            # print(to_be_deleted)
            if len(to_be_deleted) > 0:
                for t0 in to_be_deleted:
                    system.bonds.remove(t0)
            self.N_le = len(tags) - len(to_be_deleted)

        # Creating part
        system = self.system
        N_beads = len(self.stop_probs)
        beads_list = np.arange(N_beads)
        new_le_N = self.new_le_N
        tags = []
        for b in self.system.bonds:
            if b.typeid != self.le_typeid:
                continue
            tags.append(b.tag)
        ab_list = np.zeros((len(tags), 2))
        for i, t in enumerate(tags):
            b = system.bonds.get(t)
            ab_list[i, 0] = b.a
            ab_list[i, 1] = b.b
        # p = np.ones(N_beads)
        options = beads_list[:-1]
        if len(ab_list) > 0:
            le_lengths = np.diff(ab_list)[:, 0]
            not_permitted_a = ab_list[le_lengths == 1][:, 0].tolist()
            if len(not_permitted_a) > 0:
                options = [x for x in beads_list[:-1] if x not in not_permitted_a]
        new_a = np.random.choice(options, new_le_N, replace=False)
        for a0 in new_a:
            self.system.bonds.add(self.le_type, a0, a0+1)

        self.N_le = len(tags) + len(new_a)
    ##############################
    def get_loop_extrusion_tags(self, sl_typeid=1):
        tags = []
        for b in self.system.bonds:
            if b.typeid!=sl_typeid:
                continue
            tags.append(b.tag)
        return tags

    def update_loop_extrusion(self, timstep):
        system = self.system
        stop_prob_down = self.stop_probs[:, 0]
        stop_prob_up = self.stop_probs[:, 1]
        tags = self.get_loop_extrusion_tags()
        N_beads = len(self.stop_probs)
        ######
        for t in tags:
            b = self.system.bonds.get(t)
            a1 = b.a
            b1 = b.b
            rand_up, rand_down = np.random.rand(2)

            if stop_prob_up[b.b]<rand_up:
                if b.b<(N_beads-1):
                    b1 = b1 + 1
            if stop_prob_down[b.a]<rand_down:
                if b.a>0:
                    a1 = a1 - 1
            p1 = system.particles.get(a1)
            p2 = system.particles.get(b1)
            dr =  self.dist(p1, p2)
            if (dr<self.move_dist_thr)and((a1!=b.a)or(b1!=b.b)):
                system.bonds.remove(t)
                system.bonds.add(self.le_type, a1, b1)

    def get_updated_loop_extrusion(self):
        system = self.system
        tags = get_loop_extrusion_tags(system)
        ######
        res = [[]]
        for t in tags:
            b = system.bonds.get(t)
            a1 = b.a
            b1 = b.b
            res = res + [[a1, b1]]
            rand_up, rand_down = np.random.rand(2)

            if stop_prob_up[b.b]<rand_up:
                if b.b<(N_beads-1):
                    b1 = b1 + 1
            if stop_prob_down[b.a]<rand_down:
                if b.a>0:
                    a1 = a1 - 1
            p1 = system.particles.get(a1)
            p2 = system.particles.get(b1)
            dr =  dist(p1, p2)
            if (dr<self.move_dist_thr)and((a1!=b.a)or(b1!=b.b)):
                res[-1] = [a1, b1]

            #print(a1, b1)
        return np.array(res[1:])
    ##############################
    def update_loop_extrusion2(self, timestep):
        def get_le_neighbs(x, direction, up_lim):
            res = x+direction
            res[res<0] = 0
            res[res>=up_lim] = up_lim-1
            return res

        system = self.system
        sl_typeid = self.le_typeid
        stop_probs = self.stop_probs.copy()

        snap = system.take_snapshot(all=True)
        loop_extrusions_ids = snap.bonds.typeid == sl_typeid
        loop_extrusions = snap.bonds.group[loop_extrusions_ids].astype(int)
        N_loop_extrusions = loop_extrusions.shape[0]
        if loop_extrusions.shape[0]==0:
            return

        group1 = loop_extrusions.copy()

        # To avoid sli passing each others
        neighbs = get_le_neighbs(loop_extrusions[:, 0], -1, stop_probs.shape[0])
        stop_probs[neighbs, 1] = np.max([stop_probs[neighbs, 1], np.ones(len(loop_extrusions))*self.passing_p], axis=0)
        neighbs = get_le_neighbs(loop_extrusions[:, 1], +1, stop_probs.shape[0])
        stop_probs[neighbs, 0] = np.max([stop_probs[neighbs, 0], np.ones(len(loop_extrusions))*self.passing_p], axis=0)

        neighbs = get_le_neighbs(loop_extrusions[:, 0], +1, stop_probs.shape[0])
        stop_probs[neighbs, 0] = np.max([stop_probs[neighbs, 0], np.ones(len(loop_extrusions))*self.passing_p], axis=0)
        neighbs = get_le_neighbs(loop_extrusions[:, 1], -1, stop_probs.shape[0])
        stop_probs[neighbs, 1] = np.max([stop_probs[neighbs, 1], np.ones(len(loop_extrusions))*self.passing_p], axis=0)
        
        ######

        random_probs = np.random.rand(loop_extrusions.shape[0], 2)

        accept_moves = np.zeros_like(random_probs).astype(bool)
        accept_moves[:, 0] = stop_probs[loop_extrusions[:, 0], 0] < random_probs[:, 0]
        accept_moves[:, 1] = stop_probs[loop_extrusions[:, 1], 1] < random_probs[:, 1]

        check_dist_ids = np.logical_or(accept_moves[:, 0], accept_moves[:, 1])
        dist_small = np.zeros_like(check_dist_ids).astype(bool)
        
        group1[check_dist_ids, 0] -= accept_moves[check_dist_ids, 0] # go down 
        group1[check_dist_ids, 1] += accept_moves[check_dist_ids, 1] # fo up
        #group1[group1>=snap.particles.N] = snap.particles.N-1
        #group1[group1<0] = 0

        ## check if we have a distance threshold to extrude
        ## dist. thresh. -1 means no threshold!
        if self.move_dist_thr!=-1:
            dr0 = self.dist_arr(snap, group1[check_dist_ids])
            dist_small[check_dist_ids] = dr0<self.move_dist_thr
        else:
            dist_small[:] = True

        not_update_ids = np.logical_not(dist_small)

        group1[not_update_ids, 0] += accept_moves[not_update_ids, 0] # reverse go down 
        group1[not_update_ids, 1] -= accept_moves[not_update_ids, 1] # reverse fo up

        snap.bonds.group[loop_extrusions_ids] = group1
        system.restore_snapshot(snap)
        ######

