#+TITLE: HLB project

* Loop-extrusion
  The implementation in the HLB code should be updated.
** Old parameters:
   - $t_{\rm{extrusion}}$
   - Fraction of changed LE factors
   - Rate of changing LE factors
   - Total number of LE factors
** New parameters:
   Values extracted from Davide's 
   * $t_{\rm{extrusion}} = 5e4 \Delta t$
   * $k_{ex}/k_{off} = 80$
     It means that $\t_{off} = 80\times t_{\rm{extrusion}} = 4 \times 10^6$
   * Total number of LE factors.

* Simulations:
  | System             | Cleis | Ehsan          | Notes |
  |--------------------+-------+----------------+-------|
  | Active             |       | example:5r-10d |       |
  | PRC                |       |                |       |
  | LE                 |       |                |       |
  | LADs               |       |                |       |
  | Active+Wall        |       |                |       |
  | Active+LE          |       |                |       |
  | Active+PRC         |       |                |       |
  | LE+Wall            |       |                |       |
  | LE+PRC             |       |                |       |
  | Active+PRC+Wall    |       |                |       |
  | Active+PRC+LE      |       |                |       |
  | Active+Wall+LE     |       |                |       |
  | Active+PRC+Wall+LE |       |                |       |

* Analysis
** Contact/Distance maps
*** Correlations
** OCI
** Radius of gyration $R_g$
   Check the version
** 4C-like profiles
*** Correlations
** Hist-clusters:
   - Contacts
   - Closeness
   - Stability  

* Parameters:
- Specific potential: $\epsilon=8$ and $r_c=1.8$
- Background: $\epsilon_{\rm{bg}} = 2$ $r_c=1.8$

*Do we want to try different values? Smaller $\epsilon_{\rm{bg}}?*
   
