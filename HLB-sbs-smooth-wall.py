#! /usr/bin/env python3

import hoomd
import numpy as np, itertools, os, sys
from hoomd import md
import argparse
import sbs_class, saw
import ehsan_loopextrusion

parser = argparse.ArgumentParser()
parser.add_argument('--settings', type=str, help='the setting file', default='default-settings.py', dest='settings')
parser.add_argument('--sim-id', type=str, help='the simulaiton id', default='', dest='sim_id')
parser.add_argument('--resume', type=bool, help='resume (default: False)', default=False, dest='resume')
parser.add_argument('--restart-file', type=str, help='the restart file. If pass nothing, the fump file is used.', default='', dest='restart_file')
parser.add_argument('--hoomd','--hoomd_args', default='gpu', dest='hoomd_args', type=str)
args = parser.parse_args()
print(args.settings)
exec('import {} as settings'.format(args.settings[:-3]))

sbs = sbs_class.sbs(hoomd, settings)
sbs.init_world(mode=args.hoomd_args)
sbs.set_restart(True, restart_file='{}-{}.gsd'.format('restart', args.sim_id))

dump_fname = '{}-{}.gsd'.format(settings.dump_fname, args.sim_id)
restart_fname = '{}-{}.gsd'.format(settings.restart_fname, args.sim_id)
if os.path.exists(restart_fname):
    # read the restart file
    system = hoomd.init.read_gsd(filename=restart_fname, restart=restart_fname)
    restart = True #flag the simulation mode!
else:
    restart = False
    snapshot = hoomd.data.make_snapshot(**settings.snap_args)
    for pol0 in settings.polymers:
        sbs.add_random_polymer(snapshot, L=settings.L*np.array([1,1,0.9]), **pol0)
    for binders0 in settings.binders:
        sbs.add_random_particles(snapshot, L=settings.L*0.95, **binders0)

    #sbs.add_random_particles(snapshot, L=settings.L, size=10000, particles_typeid=settings.particle_types.index('laminar'))
    #wall_p_id = snapshot.particles.typeid>=settings.particle_types.index('laminar')
    #print(np.sum(wall_p_id))
    #snapshot.particles.position[-10000:,2] = +settings.L[2]*0.499
    #snapshot.particles.diameter[:] = 1.
    system = hoomd.init.read_snapshot(snapshot) # read the first snapshot

# Neighborlist
nl = md.nlist.cell(check_period=1)
nl.set_params(r_buff=0.6)

#####################
## pairs and bonds ##
#####################
sigma = 1
pairs = {}
bonds = {}
angles = {}
for k in settings.pairs.keys():
    pairs[k] = eval(settings.pairs[k]['init_cmd'])
for k in settings.bonds.keys():
    bonds[k] = eval(settings.bonds[k]['init_cmd'])
    for coeffs in settings.bonds[k]['coeff_cmds']:
        bonds[k].bond_coeff.set(**coeffs)
###########
## walls ##
###########
#r_cut = sigma*2**(1./6.)
lj_wall_rep = md.wall.lj (settings.repulsive_walls, r_cut=2**(1./6.))
lj_wall_att = md.wall.lj (settings.attractive_walls, r_cut=2**(1./6.))
lj_wall_rep.force_coeff.set(settings.particle_types, sigma=1.0, epsilon=1.0, r_cut=2**(1./6.))
lj_wall_att.force_coeff.set(settings.particle_types, sigma=1.0, epsilon=1.0, r_cut=2**(1./6.))
#lj_wall_att.force_coeff.set('laminar', sigma=1.0, epsilon=1.0, r_cut=False)
#lj_wall_rep.force_coeff.set('laminar', sigma=1.0, epsilon=1.0, r_cut=False)
###########
## remove_overlap ##
###########
kargs = settings.init_remove_overlaps(globals())
all = settings.particle_groups['all']
polymer_pg = settings.particle_groups['polymer']
if hoomd.get_step()>=settings.final_timesteps:
    sys.exit()
sbs.remove_overlaps(**kargs)

dump_gsd = hoomd.dump.gsd(dump_fname, period=settings.dump_period, group=all, overwrite=False, dynamic=['attribute', 'property', 'momentum', 'topology'])
#dump_gsd = hoomd.dump.gsd(dump_fname, period=settings.dump_period, group=polymer_pg, overwrite=False, dynamic=['attribute', 'property', 'momentum', 'topology'])
restart_gsd = hoomd.dump.gsd(restart_fname, period=settings.restart_period, group=all, overwrite=True, truncate=True, dynamic=['attribute', 'property', 'momentum', 'topology'])
##########
## thermalization
##########
for k in settings.angles.keys():
    angles[k] = eval(settings.angles[k]['init_cmd'])
    for coeffs in settings.angles[k]['coeff_cmds']:
        angles[k].angle_coeff.set(**coeffs)
group = settings.init_thermalization(globals())
normal_md = md.integrate.mode_standard(dt=settings.dt)
langevin = md.integrate.langevin(group=group, kT=1.0*settings.KT, seed=np.random.randint(1,100000+1))
langevin.set_gamma(settings.particle_types, gamma=2)

if not(restart):
    hoomd.run_upto(int(settings.thermal_timesteps*1))
    ## slip links
    if settings.N_le_agents>0:
        le_agents = ehsan_loopextrusion.LEagents(system=system, stop_probs=settings.stop_probs, N_le=settings.N_le_agents, k_new=settings.le_k_new, k_remove=settings.le_k_remove,
                                              move_dist_thr=settings.le_extr_dist_thr, change_period=settings.change_le_period, update_period=settings.le_period)
        le_agents.le_type = 'loop_extrusion'
        le_agents.le_typeid = settings.bond_types.index(le_agents.le_type)
        replace_loop_extrusion = hoomd.analyze.callback(callback = le_agents.replace_loop_extrusion_factors, period=settings.change_le_period)
        loop_extrusion = hoomd.analyze.callback(callback = le_agents.update_loop_extrusion2, period=settings.le_period)
        snap = system.take_snapshot(all=True)
        ctcf = settings.stop_probs.copy()
        ctcf[:, 1] *= 1.5
        snap.particles.charge[:settings.pol_size] = np.sum(ctcf, axis=1)
        N_le_agents = settings.N_le_agents
        snap.bonds.resize(snap.bonds.N+N_le_agents)
        le_indices = np.random.choice(settings.pol_size-3, N_le_agents, replace=False)
        for i, a1 in enumerate(le_indices):
            snap.bonds.group[-i-1] = [a1,a1+2]
            snap.bonds.typeid[-i-1] = settings.le_typeid
            print(i+1, a1, a1+1)
        print('bonds = %d'%snap.bonds.N)
        system.restore_snapshot(snap)

    hoomd.run_upto(settings.thermal_timesteps)
##############################
## normal MD: attraction ##
##############################
settings.init_main_md(globals())
settings.system = system
snap0 = system.take_snapshot()
settings.orig_typeids = snap0.particles.typeid[:]
change_binders = hoomd.analyze.callback(callback = settings.change_binders, period = settings.change_period)

hoomd.run_upto(settings.final_timesteps)

restart_gsd.write_restart()
